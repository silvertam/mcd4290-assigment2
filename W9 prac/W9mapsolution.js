let map ;
let myArray=[]
if('geolocation' in navigator) { 
  navigator.geolocation.getCurrentPosition(success, error) 
} else { 
  error() 
} 
 
function success (location){ 
    
    mapboxgl.accessToken = 'pk.eyJ1IjoiaXNhZGtmbGEiLCJhIjoiY2t4NnZ1anN0MDV2bjJxcnlvMGxzaDU0biJ9.C-lu6XKCtcJ7lu7GkoE9hQ'; 
    console.log(location) 
     map = new mapboxgl.Map({ 
            container: 'map',//container id  
            style: 'mapbox://styles/mapbox/streets-v10',// style URL 
            zoom: 9,//starting zoom   
            center: [location.coords.longitude, location.coords.latitude] // starting position [lng, lat ] 
        }); 
     bounds()
}
function bounds(){
    for (let i =0 ; i < myArray.length; i++){
        
    }
    myArray = []
    
    //Task 3
    let Boundaries = map.getBounds() 
    
    
    
    
    //Task 4 
    let latlng = Boundaries._ne.lat + "," + Boundaries._ne.lng + "," + Boundaries._sw.lat + "," + Boundaries._sw.lng  
    let QueryString = "https://api.waqi.info/map/bounds/?token=303608e879e1af6eaa6f235aa8dcf2c232c279e0&latlng="+ latlng +"&callback=display" 
     
    let script = document.createElement("script") 
    script.src = QueryString 
    document.body.appendChild(script) 
     
} 
 
function display(result){ 
    
    for(let i =0; i< result.data.length;i++){
        myArray.push( new mapboxgl.Marker({
        color: AQIColour(result.data[i].aqi),
        }).setLngLat([result.data[i].lon, result.data[i].lat])
        .addTo(map));
        
        new mapboxgl.Popup({offset: 30})
        .setLngLat([result.data[i].lon, result.data[i].lat])
        .setHTML("<h7>"+result.data[i].station.name +"</h7>")
        .addTo(map); // add the marker to the map
        
    }
    map.on('zoomend',function(){
     bounds()
    });
    
    
} 
function AQIColour(aqi){
    let colour
    
    if (aqi > 0 && aqi < 50){
        colour = "green"
    }else if(aqi> 51 && aqi < 100){
        colour = "yellow"     
    }else if(aqi> 101 && aqi < 150){
        colour = "orange"
    }else if(aqi> 151 && aqi < 200){
        colour = "red"
    }else if(aqi> 201 && aqi < 300){
        colour = "purple"
    }else if(aqi> 301 && aqi < 500){
        colour = "maroon"
    }else{
        colour = 'white'
    }
    
    return colour
}
 
function error() {  
    alert("Geolocation is not avilable") 
} 
