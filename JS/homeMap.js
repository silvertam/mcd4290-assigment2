"use strict";        
mapboxgl.accessToken = "pk.eyJ1IjoiaXNhZGtmbGEiLCJhIjoiY2t4NnZ1anN0MDV2bjJxcnlvMGxzaDU0biJ9.C-lu6XKCtcJ7lu7GkoE9hQ";
        let   userlocation =[]; // user current location 
        let   myArray=[];       // POI array
        let   reverseArray=[]; 
        let   Popup = new mapboxgl.Popup();
        let    mySecondArray=[]; // popup
        let coordinates = document.getElementById('coordinates');
        var description = '<button onclick= "Addstop()" + { this.handlePop } + > Button</button>'

        let map = new mapboxgl.Map({ 
        container: 'map',//container id  
        style: 'mapbox://styles/mapbox/streets-v10',// style URL 
        zoom: 14,//starting zoom   
        center: [144.9721856,-37.7290752]})

        

    function VacationRoute(){   
                for (let i=0 ; i< userlocation.length-1;i++){
                console.log(userlocation[i+1].Latitude)
               sendXMLRequestForRoute(userlocation[i].Latitude,userlocation[i].Longitude,userlocation[i+1].Latitude,userlocation[i+1].Longitude,
                                      DrawLine)} 
        }
        function DrawLine(results)
        {
            let wavepoint = [];
            for(let i =0; i<results.routes[0].geometry.coordinates.length; i++){
                wavepoint.push(results.routes[0].geometry.coordinates[i])
            }
            map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                    'type': 'LineString',
                    'coordinates':wavepoint
                    }
                    }
                });
            
            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                'line-join': 'round',
                'line-cap': 'round'
                },
                'paint': {
                'line-color': '#888',
                'line-width': 5
                }
                });
}
        function removeLayerWithId(idToRemove)
        {
            let hasPoly = map.getLayer(idToRemove)
            if (hasPoly !== undefined)
            {
                map.removeLayer(idToRemove)
                map.removeSource(idToRemove)
            }
        }

                       
let marker = new mapboxgl.Marker({
                draggable: true
                })
                .setLngLat([144.9721856,-37.7290752])
                .addTo(map);

function onDragEnd() {
    let lngLat = marker.getLngLat();
    reverseArray = []
    reverseArray.push(lngLat.lng,lngLat.lat);
    coordinates.style.display = 'block';
    coordinates.innerHTML = `Longitude: ${lngLat.lng}<br />Latitude: ${lngLat.lat}`;
    console.log(reverseArray)
} 
marker.on('dragend', onDragEnd);


    function ConfirmSelection(){
        let text ="Start Your Happy Vacation Plan?";
        let x = document.getElementById("myDIV");
        if(confirm(text) == true){
             x.style.display = "block";
            text ="";
            document.getElementById("notes").innerHTML = text;
            
        } else{
            text ="You canceled!";
            document.getElementById("notes").innerHTML = text;
            x.style.display = "none";
        } 
        getInf()
}
    function getInf(){
        let locationRef = document.getElementById("StartingLocation").value;
        locationRef = ["a","c"];
        let dateRef = document.getElementById("Date").value;
        let carRef = document.getElementById("Vehicle").value;
        vacation.push(new Vehicle(carRef));
        //updateLSData(APP_DATA,inf);
        vacation.push(new Vacation(locationRef,"",carRef,dateRef));
        updateLSData(APP_DATA, vacation);
    
         if(locationRef == null)
    {
        return;
    }
        else if(locationRef ==""){
            // add to vacation
        vacation.addLocation(locationRef);
        // update LS
        updateLSData(APP_DATA, vacation);
        }
        
    }

function searchLocation(){
    let locationRef = document.getElementById("StartingLocation").value;
    sendWebServiceRequestForForwardGeocoding(locationRef,"ForwardDisplay")
}


function ForwardDisplay(results){
    let SelfLng=results.results[0].geometry.lng
    let SelfLat=results.results[0].geometry.lat
    
    map.panTo([SelfLng,SelfLat])
    
    let LatLng = new LocationSpot(SelfLng,SelfLat)    
    userlocation.push(LatLng)
    console.log(userlocation)
    }

    function Addstop(){
        let POIRef = document.getElementById("nextLoc").value
        document.getElementById("cuurentAddr").value = POIRef
        document.getElementById("nextLoc").value = ''
        getDistanceFromLatLonInKm()
        sendWebServiceRequestForForwardGeocoding(POIRef,"AddArray")
    }
    function AddArray(results){
        let POILng = results.results[0].geometry.lng
        let POILat = results.results[0].geometry.lat
        let Distance = 0
        userlocation.push(new LocationSpot(POILng,POILat))
        for(let i =0 ; i< userlocation.length-1;i++){
        let ULlat0=userlocation[i].Latitude
        let ULlon0=userlocation[i].Longitude
        let ULlat1=userlocation[i+1].Latitude
        let ULlon1=userlocation[i+1].Longitude
        
        Distance = getDistanceFromLatLonInKm(ULlat0,ULlon0,ULlat1,ULlon1)
        }
        let Capacity= vacation[0]._fuelCapacity
        let RemainDis = Capacity -Distance
        document.getElementById("distance").value = Distance
        document.getElementById("RemainDis").value = RemainDis
    }

        function dragSearch(){
            let lng = reverseArray[0];
            let lat = reverseArray[1];
            sendWebServiceRequestForReverseGeocoding(lat,lng,"ReverseDisplay")
        }
        function ReverseDisplay(grapresult){
            document.getElementById("StartingLocation")= grapresult.results[0].formatted
            let LatLng ={
            Longitude: result.coords.longitude,
            Latitude: result.coords.latitude
        }
                userlocation.push(LatLng)
                console.log(userlocation)
        }


// The codes below are for the filter page nearby the location.
function MyLocation(){
         
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(displaySelf);
      } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
      }

 }
function displaySelf(result){
        console.log(result)
        map.panTo([result.coords.longitude,result.coords.latitude])
        
        let LatLng ={
            Longitude: result.coords.longitude,
            Latitude: result.coords.latitude
        }
    userlocation.push(LatLng)
    console.log(userlocation)
}
     
   
    function Fuel(){
        let LastPOI = userlocation[userlocation.length- 1]
        console.log(LastPOI)
        removemaker()
        sendXMLRequestForPlaces("gas station",LastPOI.Longitude,LastPOI.Latitude,displayPOI)
    }

    function Restaurant(){
        let LastPOI = userlocation[userlocation.length- 1]
        removemaker()
        sendXMLRequestForPlaces("food",LastPOI.Longitude,LastPOI.Latitude,displayPOI)
    }
    
    function Attraction(){
        let LastPOI = userlocation[userlocation.length- 1]
        removemaker()
        sendXMLRequestForPlaces("attraction",LastPOI.Longitude,LastPOI.Latitude,displayPOI)
    }
    function Hotel(){
        let LastPOI = userlocation[userlocation.length- 1]
        removemaker()
        sendXMLRequestForPlaces("lodging",LastPOI.Longitude,LastPOI.Latitude,displayPOI)
    }

    function removemaker(){
        for(let i =0; i< myArray.length;i++){
        myArray[i].remove()
        mySecondArray[i].remove()
    }   myArray=[]
        mySecondArray=[]
    }
    function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
      let R = 6371; 
      var dLat = deg2rad(lat2-lat1);  
      var dLon = deg2rad(lon2-lon1); 
      var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ; 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c; // Distance in km
      return d;
    }

    function deg2rad(deg) {
      return deg * (Math.PI/180)
    }
     

    // displaying the points of interest with marker and popup
    function displayPOI(result) {
        console.log(result)  
        for(let i =0; i< result.features.length;i++){
        myArray.push( new mapboxgl.Marker({
        color: "red",})
        .setLngLat([result.features[i].center[0],result.features[i].center[1]])
        .addTo(map));
        
        mySecondArray.push( new mapboxgl.Popup({offset: 30})
        .setLngLat([result.features[i].center[0],result.features[i].center[1]])
        .setHTML("<h10>"+i+"   "+result.features[i].place_name +"</h10><br>"+ description) 
        .addTo(map)); // add the marker to the map 
        }
        
    }

      map.on("zoomend",function(){
        //bounds();
      })